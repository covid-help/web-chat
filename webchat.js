
window.CovidHelpBot = window.CovidHelpBot || (function () {

  class CovidHelpBot {

    constructor() {
      // ***********************
      // Variables
      // ***********************
      // Url
      this.mediaDomain = 'https://covidhelpbot.cisforce.com';
      this.botServerHost = 'https://covidhelp-api.cisforce.com';
      this.imgUrl = `${this.mediaDomain}/assets/img`;
      this.checkLocalStylesForTest = true;
      // Init
      this.autoInit = true;
      // User data
      this.userData = null;
      // Check body interval
      this.bodyInterval = null;
      this.checkBodyIntervalCount = 0;
      this.checkBodyIntervalMaxCount = 100;
      // Open timer
      this.autoOpen = true;
      this.autoOpenTimeout = 6000;
      this.autoOpenTimer = null;
      this.autoOpenTimeoutDone = false;
      // Eval interval
      this.evalIntervalCount = 0;
      this.evalIntervalMaxCount = 3;
      this.evalInterval = null;
      // Microsoft webchat
      this.directLineToken = 'JMVYRXDJbeY.rlPZ6I5ollkrsj6_uhP1B_ZVe4ZFa1CC4d59TFJTTuk';
      this.webchatInterval = null;
      this.webchatIntervalCount = 0;
      this.webchatIntervalMaxCount = 100;
      this.directLine = null;
      this.webchatInitRetryTimeout = 3000;
      this.postInitActivityCount = 0;
      this.postInitActivityTimeout = 5000;
      this.postInitActivityMaxCount = 25;
      this.postInitActivitySuccessful = false;
      this.postInitActivityState = 'initial'; // 'initial' | 'connecting' | 'retrying' | 'success' | 'failed'
      // Web elements
      this.openButton = null;
      this.chatWindow = null;
      this.chatOpened = false;
      this.chatWindowPosition = 'right';
      // Other variables
      this.defaultIntervalTimeout = 250;
      this.scrollChatOnFirstMessage = true;
      this.firstMessageText = 'Test';
      this.scrollChatToTopInterval = null;

      // ***********************
      // Params format:
      // {
      //   autoInit: boolean
      //   autoOpen: boolean
      //   autoOpenTimeout: number (ms)
      //   chatWindowPosition: 'left' | 'right'
      // }
      // ***********************

      // Check body
      this.bodyInterval = setInterval(() => {
        this.checkBodyIntervalCount++;
        if (document.body || this.checkBodyIntervalCount >= this.checkBodyIntervalMaxCount) {
          clearInterval(this.bodyInterval);

          // Set params
          if (this.params) {
            Object.assign(this, this.params);
          }

          if (this.autoOpen) {
            this.autoOpenTimer = setTimeout(() => {
              this.autoOpenTimeoutDone = true;
              this.autoOpenChat();
            }, this.autoOpenTimeout);
          }

          if (this.autoInit) {
            this.init();
          }
        }
      }, this.defaultIntervalTimeout);
    }

    // ***********************
    // Init
    // ***********************

    init() {
      this.initStyles();
      this.openButton = document.createElement('button');
      this.openButton.className = `webchat__toggle_button webchat__toggle_button_${this.chatWindowPosition}`;
      this.openButton.innerHTML = `<span class="webchat__icon webchat__chat"><img src="${this.imgUrl}/icon-button.png"></span>`;
      this.openButton.innerHTML += `<span class="webchat__icon webchat__close"><svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="32" height="32" viewBox="0 0 32 32" style=" fill:#ffffff;"><path d="M 7.21875 5.78125 L 5.78125 7.21875 L 14.5625 16 L 5.78125 24.78125 L 7.21875 26.21875 L 16 17.4375 L 24.78125 26.21875 L 26.21875 24.78125 L 17.4375 16 L 26.21875 7.21875 L 24.78125 5.78125 L 16 14.5625 Z"></path></svg></span>`;
      document.body.appendChild(this.openButton);
      this.openButton.addEventListener('click', this.toggleChat.bind(this));

      let headerCloseButton = document.createElement('button');
      headerCloseButton.className = `webchat__header_icon_close`;
      headerCloseButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="20" height="20" viewBox="0 0 32 32" style=" fill:#555;"><path d="M 7.21875 5.78125 L 5.78125 7.21875 L 14.5625 16 L 5.78125 24.78125 L 7.21875 26.21875 L 16 17.4375 L 24.78125 26.21875 L 26.21875 24.78125 L 17.4375 16 L 26.21875 7.21875 L 24.78125 5.78125 L 16 14.5625 Z"></path></svg>`;
      headerCloseButton.addEventListener('click', this.toggleChat.bind(this));

      let header = document.createElement('div');
      header.className = `webchat__header`;
      header.innerHTML = `
          <span><b>Covid Help</b></span>
          <span class="webchat__small">This is <a href="https://covidhelpbot.cisforce.com/" target="_blank">how to install</a> the same chat on your website</span>
      `;
      header.appendChild(headerCloseButton);

      let webchat = document.createElement('div');
      webchat.id = `webchat__webchat`;
      webchat.setAttribute('role', 'main');

      let loader = document.createElement('div');
      loader.id = `webchat__loader`;
      loader.style.display = "none";
      loader.innerHTML = 'Loading, please wait...';

      this.chatWindow = document.createElement('div');
      this.chatWindow.className = `webchat__chat_window webchat__chat_window_${this.chatWindowPosition}`;
      this.chatWindow.appendChild(header);
      this.chatWindow.appendChild(webchat);
      this.chatWindow.appendChild(loader);
      document.body.appendChild(this.chatWindow);

      const savedUserString = localStorage.getItem('covid-help-user');
      const user = savedUserString ? JSON.parse(savedUserString) : { id: this.uuidv4(), name: 'Web Chat User' };
      const userIdParam = this.getParameterByName('userId');
      if (userIdParam) {
        user.id = userIdParam;
        if (window.history.replaceState) {
          window.history.replaceState({}, document.title, this.removeURLParameter('userId'));
        }
      }
      localStorage.setItem('covid-help-user', JSON.stringify(user));
      const ipAddress = this.getIpAddress();
      this.userData = { session: { id: this.uuidv4(), source: location.host, ip: ipAddress }, user };

      const microsoftWebchatScript = this.loadFile('microsoft-webchat.min.js');
      this.runWebChatScript(microsoftWebchatScript);
      if (!window.WebChat) {
        this.evalInterval = setInterval(() => {
          this.evalIntervalCount++;
          if (!!window.WebChat || this.evalIntervalCount >= this.evalIntervalMaxCount) {
            clearInterval(this.evalInterval);
          } else {
            this.runWebChatScript(xmlHttp.responseText);
            if (!window.WebChat) {
              const script = document.createElement('script');
              script.src = `${this.mediaDomain}/microsoft-webchat.min.js`;
              document.head.appendChild(script);
            }
          }
        }, this.webchatInitRetryTimeout);
      }
      this.webchatInterval = setInterval(() => {
        this.webchatIntervalCount++;
        if (!!window.WebChat || this.webchatIntervalCount >= this.webchatIntervalMaxCount) {
          clearInterval(this.webchatInterval);
          this.directLine = window.WebChat.createDirectLine({
            token: this.directLineToken
          });
          const store = window.WebChat.createStore(
            {},
            ({ dispatch }) => next => action => {
              if (action.type === 'DIRECT_LINE/INCOMING_ACTIVITY' && this.scrollChatOnFirstMessage === true && this.getNestedOrNull(['payload', 'activity', 'attachments', 0, 'content', 'text'], action) === this.firstMessageText) {
                this.scrollChatOnFirstMessage = false;
                this.scrollChatToTop();
              }
              if (action.type === 'DIRECT_LINE/POST_ACTIVITY_FULFILLED') {
                this.scrollChatToLastMessage();
              }
              if (action.type === 'WEB_CHAT/SEND_MESSAGE') {
                this.scrollChatOnFirstMessage = false;
                clearInterval(this.scrollChatToTopInterval);
              }
              return next(action);
            }
          );
          window.WebChat.renderWebChat(
            {
              directLine: this.directLine,
              userID: this.userData.user.id,
              username: JSON.stringify(this.userData),
              locale: 'en-US',
              store,
              styleOptions: {
                hideUploadButton: true,
                botAvatarInitials: 'VC',
                userAvatarInitials: 'US',
                botAvatarImage: `${this.imgUrl}/avatar-bot.png`,
                userAvatarImage: `${this.imgUrl}/avatar-user.png`
              }
            },
            document.getElementById('webchat__webchat')
          );
          this.directLine.connectionStatus$
            .subscribe(connectionStatus => {
              switch(connectionStatus) {
                case 1:
                  this.postInitActivity();
                  break;
                }
            });
        }
      }, this.defaultIntervalTimeout);
    }

    initStyles() {
      if (this.checkLocalStylesForTest && (!location.host || location.host.includes('localhost'))) {
        const style = document.createElement('link');
        style.rel = 'stylesheet';
        style.href = './webchat.css';
        document.getElementsByTagName('head')[0].appendChild(style);
      } else {
        const styles = this.loadFile('webchat.css');
        const style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = styles;
        document.getElementsByTagName('head')[0].appendChild(style);
      }
    }

    runWebChatScript(script) {
      if (script) {
        window.__define = window.define;
        window.__require = window.require;
        window.define = undefined;
        window.require = undefined;
        eval(script);
        window.define = window.__define;
        window.require = window.__require;
        window.__define = undefined;
        window.__require = undefined;
        console.log('Attempt to load Covid Help Bot. Result: ' + !!window.WebChat);
      }
    }

    // ***********************
    // Webchat events
    // ***********************

    postInitActivity() {
      if (this.postInitActivityState === 'initial') {
        this.setPostInitActivityState('connecting');
      }
      if (this.postInitActivityCount < this.postInitActivityMaxCount) {
        this.postInitActivityCount++;
        if (this.postInitActivityCount >= 3 && this.postInitActivityState === 'connecting') {
          this.setPostInitActivityState('retrying');
        }
        this.directLine.postActivity({
          from: this.userData.user,
          type: 'event',
          name: 'webchat/join',
          value: { locale: 'en-US' }
        }).subscribe((res) => {
          if (res === 'retry') {
            console.log(`Covid Help Bot init failed. Retry required.`);
            setTimeout(() => {
              this.postInitActivity();
            }, this.postInitActivityTimeout);
          } else {
            console.log('Covid Help Bot connected successfully.');
            this.setPostInitActivityState('success');
            this.postInitActivitySuccessful = true;
            this.autoOpenChat();
          }
        });
      } else {
        this.setPostInitActivityState('failed');
      }
    }

    setPostInitActivityState(state) {
      if (state !== this.postInitActivityState) {
        this.postInitActivityState = state;
        const loader = document.getElementById('webchat__loader');
        if (state === 'retrying') {
          if (loader) {
            loader.innerHTML = 'Retrying connection, please wait...';
          }
        } else if (state === 'success') {
          if (loader && loader.parentNode) {
            loader.parentNode.removeChild(loader);
          }
        } else if (state === 'failed') {
          if (loader) {
            loader.innerHTML = 'Connection failed. We are sorry. Please try to refresh the page or try later.';
          }
        }
      }
    }

    toggleChat() {
      if (this.autoOpenTimer) {
        clearTimeout(this.autoOpenTimer);
        this.autoOpenTimer = null;
      }
      if (this.openButton.className.indexOf('active') === -1) {
        this.openButton.className += ' webchat__active';
        this.chatWindow.className += ' webchat__active';
      } else {
        this.openButton.className = this.openButton.className.replace(' webchat__active', '');
        this.chatWindow.className = this.chatWindow.className.replace(' webchat__active', '');
      }
      this.chatOpened = !this.chatOpened;
    }

    autoOpenChat() {
      if (this.postInitActivitySuccessful && this.autoOpen && this.autoOpenTimeoutDone && !this.chatOpened) {
        this.toggleChat();
      }
    }

    scrollChatToTop() {
      let scrollChatToTopIntervalCount = 0;
      this.scrollChatToTopInterval = setInterval(() => {
        scrollChatToTopIntervalCount++;
        if (this.chatWindow.querySelector('ul[role="list"]').firstChild && document.querySelector('ul[role="list"]').parentElement.scrollTop !== 0) {
          clearInterval(this.scrollChatToTopInterval);
          this.chatWindow.querySelector('ul[role="list"]').firstChild.scrollIntoView({behavior: 'smooth', block: 'start'});
          setTimeout(() => { document.querySelector('ul[role="list"]').parentElement.scrollTop = 0; }, 200);
        } else if (scrollChatToTopIntervalCount >= 20) {
          clearInterval(this.scrollChatToTopInterval);
        }
      }, 500);
    }

    scrollChatToLastMessage() {
      this.chatWindow.querySelector('ul[role="list"]').lastChild.scrollIntoView({behavior: 'smooth', block: 'start'});
    }

    // ***********************
    // Utils
    // ***********************

    loadFile(file) {
      const xmlHttp = new XMLHttpRequest();
      xmlHttp.open('GET', `${this.mediaDomain}/` + file, false); // false for synchronous request
      xmlHttp.send(null);
      return xmlHttp.responseText;
    }

    getIpAddress() {
      let ip = null;
      try {
        const xmlHttp = new XMLHttpRequest();
        xmlHttp.open('GET', `${this.botServerHost}/api/utils/ip-address`, false); // false for synchronous request
        xmlHttp.send(null);
        ip = JSON.parse(xmlHttp.responseText).ip;
      } catch (ex) {}
      return ip;
    }

    uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }

    getParameterByName(name) {
      const url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
      const results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    removeURLParameter(parameter) {
      const url = window.location.href;
      //prefer to use l.search if you have a location/link object
      const urlparts = url.split('?');   
      if (urlparts.length >= 2) {
        const prefix = encodeURIComponent(parameter) + '=';
        const pars = urlparts[1].split(/[&;]/g);
        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {    
          //idiom for string.startsWith
          if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
            pars.splice(i, 1);
          }
        }
        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
      }
      return url;
    }

    getNestedOrNull(path, object) {
      return path.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, object);
    }
  }

  return new CovidHelpBot();
}());
